#!/usr/bin/env python3

import sys
import argparse
import syslog
import itertools
import os
import pwd
import grp
import stat
from netmiko import ConnectHandler
from netmiko import SCPConn
from netmiko.ssh_dispatcher import CLASS_MAPPER_BASE
from datetime import datetime
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

# Default waiting timer for each remoter command
SLEEP = 1
CPREF = '$$'
TFILE = '/shared/tmp/{}_autobackup'.format(
        os.path.basename(sys.argv[0]).replace('.', '_'))
NETMIKO_USERNAME = 'username'
NETMIKO_PASSWORD = 'password'
NETMIKO_DEVICE_TYPE = 'device_type'
NETMIKO_IP = 'ip'
NETMIKO_VERBOSE = 'verbose'
NETMIKO_CONN_PARAS = {
        NETMIKO_USERNAME: [
            ['-u', '--{}'.format(NETMIKO_USERNAME)],
            {'required': True,
             'help': 'Remote device login username'}],
        NETMIKO_PASSWORD: [
            ['-p', '--{}'.format(NETMIKO_PASSWORD)],
            {'required': True}],
        NETMIKO_IP: [
            ['-i', '--{}'.format(NETMIKO_IP)],
            {'required': True,
             'help': 'Remove device IP address'}],
        NETMIKO_VERBOSE: [
            ['--{}'.format(NETMIKO_VERBOSE)],
            {'action': 'store_true',
             'help': 'Set verbose output in ssh connection'}],
        NETMIKO_DEVICE_TYPE: []
}


def set_args(dn=None):
    now = datetime.now()
    parser = argparse.ArgumentParser()
    for _, v in NETMIKO_CONN_PARAS.items():
        if v:
            parser.add_argument(*v[0], **v[1])
    parser.add_argument('-f', '--filename', required=True,
                        help='Save to the given path.')
    parser.add_argument('-n', '--device_id', required=True,
                        help='Remote device type', choices=dn)
    parser.add_argument('-o', '--fileowner',
                        default='',
                        help='Assign the configuration file owner')
    parser.add_argument('--tempfile',
                        default=TFILE,
                        help='Save configuration in the remote network device '
                             'with this path, for F5 LTM like product. '
                             'The default setting is {}'.format(TFILE))
    parser.add_argument('--cprefix',
                        default=CPREF,
                        help='Set commands output prefix.'
                             'Empty string means, no commands printing. '
                             'The default setting is {}'.format(CPREF))
    parser.add_argument('--delay',
                        type=float,
                        default=SLEEP,
                        help='Set delay second before each device command, '
                             'The default delay is {}'.format(SLEEP))
    parser.add_argument('--type_list', action='store_true',
                        help='Only show all device type supported by Netmiko')
    args = parser.parse_args()
    return args


class Device_factory():

    def __init__(self):
        self.connectors = self.get_connectors()

    def get_connectors(self):
        _ = dict([(sc._id, sc) for sc in Connector.__subclasses__()])
        return _

    def create(self, device_id=None, **kwargs):
        connector = self.connectors.get(device_id)
        if not connector:
            raise ValueError(device_id)
        return connector(**kwargs)


class Connector():
    _cli = None
    _type = None
    _id = None

    def __init__(self, **kwargs):
        self.cli = self.get_cli()
        self.device_type = self.get_device_type()
        self.device_id = self.get_device_id()
        self.paras = kwargs.copy()
        self.paras[NETMIKO_DEVICE_TYPE] = self.device_type
        self.conn_paras = {key: self.paras[key] for key in NETMIKO_CONN_PARAS}
        self.net_connect = None

    @classmethod
    def get_cli(cls):
        return cls._cli

    @classmethod
    def get_device_type(cls):
        return cls._type

    @classmethod
    def get_device_id(cls):
        return cls._id

    def download_config(self):
        try:
            self.net_connect = ConnectHandler(**self.conn_paras)
            # net_connect.enable()
            # net_connect.send_command('terminal length 0')
            _out = [self.net_connect.send_command(c) for c in self.cli]
        except KeyboardInterrupt as err:
            self.close_conn()
            raise
        # print(_out[:300])
        self.config = _out
        syslog.syslog('@{} following commands have been ran [{}]'.format(
            self.conn_paras['ip'],
            ', '.join(self.cli)))

    def close_conn(self):
        if self.net_connect:
            self.net_connect.disconnect()

    def _write_config(self, filename=None, ip=None, cprefix=None, **kwargs):
        _conf = self.config
        if cprefix:
            _zipper = zip([cprefix + c for c in self.cli], self.config)
            _conf = list(itertools.chain(*_zipper))
        with open(filename, 'w') as f:
            f.write(os.linesep.join(_conf))
        print("%s: Saved the configuration from %s to %s" %
              (datetime.now().strftime("%b %d %H:%M"), ip, filename))

    def _change_priv(self, fileowner=None, filename=None, **kwargs):
        if fileowner and os.path.isfile(filename):
            os.chmod(filename,
                     stat.S_IRUSR+stat.S_IWUSR+stat.S_IWGRP+stat.S_IRGRP)
            uid = pwd.getpwnam(fileowner).pw_uid
            gid = grp.getgrnam(fileowner).gr_gid
            os.chown(filename, uid, gid)

    def run(self):
        self.download_config()
        self.close_conn()
        self._write_config(**self.paras)
        self._change_priv(**self.paras)


class Cisco(Connector):
    _cli = ('show running-config',)
    _type = 'cisco_ios'
    _id = 'CISCO'

    def __init__(self, **kwargs):
        super(type(self), self).__init__(**kwargs)
        # super(Cisco, self).__init__(**kwargs)
        # print(Cisco.get_cli())


class Cisco_show_int(Connector):
    _cli = ('show ip interface brief', 'show interface fa 0/0', 'show loging')
    _type = 'cisco_ios'
    _id = 'CISCO_INT'

    def __init__(self, **kwargs):
        super(type(self), self).__init__(**kwargs)


class Junos(Connector):
    _cli = ('show configuration | no-more',)
    _type = 'juniper_junos'
    _id = 'JUNOS'

    def __init__(self, **kwargs):
        super(type(self), self).__init__(**kwargs)


class F5_LTM_POOL(Connector):
    _cli = ('tmsh show ltm virtual juniper', 'tmsh show pool', )
    _type = 'linux'
    _id = 'F5_LTM_POOL'

    def __init__(self, **kwargs):
        super(type(self), self).__init__(**kwargs)


class F5_LTM(Connector):
    _cli = ('tmsh save /sys ucs {}',)
    _type = 'linux'
    _id = 'F5_LTM'
    tfile = '/shared/tmp/f5_auto_backup'

    def __init__(self, **kwargs):
        super(type(self), self).__init__(**kwargs)
        self.cli = self.cli_setting(**kwargs)
        self.f5_suffix = '.ucs'
        self.keyword = ' is saved'

    def cli_setting(self, temp_file=tfile, **kwargs):
        return [c.format(temp_file) for c in self._cli]

    def ans_check(self):
        ans = False
        for o in self.config:
            if self.keyword in o:
                ans = True
        return ans

    def scp_config(self, temp_file=tfile, filename=None, **kwargs):
        _f = temp_file + self.f5_suffix
        scp_conn = SCPConn(self.net_connect)
        scp_conn.scp_get_file(_f, filename)
        syslog.syslog('scp ucs file from {} to {}'.format(
            self.conn_paras['ip'], filename))

    def run(self):
        self.download_config()
        if self.ans_check():
            self.scp_config(**self.paras)
            self._change_priv(**self.paras)
        self.close_conn()


if __name__ == "__main__":
    factory = Device_factory()
    args = set_args(factory.connectors.keys())
    if args.type_list:
        print(os.linesep.join(CLASS_MAPPER_BASE.keys()))
    else:
        device = factory.create(**args.__dict__)
        device.run()
