Dump networking device configuration with Linux CLI.
It is very handy to run it in cron table. If you don't want to get a configuration with Ansible or any fancy configuration download tool. By default, it support Cisco IOS like, Junos and F5 LTM/GTM device configuration download.

**Install**

Now it have to run in Linux with Python3

1. git clone this repo
2. cd confdump
3. pip3 install -r require.txt

**Example**

$ cd /home/USER/confdump
$ ./confdump.py -u reader -p readonly -i 192.168.1.100 -f /tmp/r1_$(date "+%Y%m%d").conf -n "CISCO"

In crontab file need to change the nested date command like $(date "+\%Y\%m\%d")
